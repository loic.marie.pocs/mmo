var game = new Phaser.Game("100%", "100%", Phaser.AUTO, 'game', { preload: preload, create: create, update: update })

function preload() {
    // game.load.image('forestGround', 'assets/forest_ground.png')
    // game.load.spritesheet('city', 'assets/grass.png', 32, 32, 18)
    game.load.tilemap('map', 'assets/map/lvl1.csv', null, Phaser.Tilemap.CSV);
    game.load.image('tiles', 'assets/map/grass.png');

    game.load.spritesheet('bot', 'assets/characters/BODY_male.png', 64, 64, 36);
}

var map;
var layer;
var bot;
var cursors;

function create() {

    //  Because we're loading CSV map data we have to specify the tile size here or we can't render it
    map = game.add.tilemap('map', 32, 32);

    //  Now add in the tileset
    map.addTilesetImage('tiles');

    //  Create our layer
    layer = map.createLayer(0);

    //  Resize the world
    layer.resizeWorld();

    //  Allow cursors to scroll around the map
    cursors = game.input.keyboard.createCursorKeys();

    // var help = game.add.text(16, 16, 'Arrows to scroll', { font: '14px Arial', fill: '#ffffff' });
    // help.fixedToCamera = true;



    //  This sprite is using a texture atlas for all of its animation data
    bot = game.add.sprite(200, 200, 'bot')
    bot.animations.add('idle', [18]);
    bot.animations.add('walkUp', [0,1,2,3,4,5,6,7,8]);
    bot.animations.add('walkLeft', [9,10,11,12,13,14,15,16,17]);
    bot.animations.add('walkDown', [18,19,20,21,22,23,24,25,26]);
    bot.animations.add('walkRight', [27,28,29,30,31,32,33,34,35]);

    //  And this starts the animation playing by using its key ("walk")
    //  30 is the frame rate (30fps)
    //  true means it will loop when it finishes
}

var expX=null, expY=null;

socket.on('connect', function(data) {
  console.log('connected')
    socket.on('action', (data) => {
      if (!bot) return;
      var dpos = 100;
      if (data == 'RIGHT\n') {
        expX = bot.x + dpos;
      } else if (data == 'LEFT\n') {
        expX = bot.x - dpos;
      } else if (data == 'UP\n') {
        expY = bot.y - dpos;
      } else if (data == 'DOWN\n') {
        expY = bot.y + dpos;
      } else {
        expX = bot.x;
        expY = bot.y;
      }
      console.log(expX, expY, data);
    })
});


function update() {
    if (expX == null) expX = bot.x;
    if (expY == null) expY = bot.y;
    var dpos = 6;
    // console.log('!!', bot.x, expX, bot.y, expY)
    if (bot.x == expX && bot.y == expY) {
        bot.animations.play('idle', 15, true);
    } else {
      if (bot.x > expX) {
        moveBot('LEFT', dpos)
      } else if (bot.x < expX) {
        moveBot('RIGHT', dpos)
      } else if (bot.y > expY) {
        moveBot('UP', dpos)
      } else if (bot.y < expY) {
        moveBot('DOWN', dpos)
      } else {
        moveBot('STOP', dpos);
      }
    }
}

function moveBot(direction, dpos) {
    if (direction == 'LEFT') {
        // game.camera.x -= dpos;
        // game.camera.x = Math.max(expX, game.camera.x);
        bot.x -= dpos;
        bot.x = Math.max(expX, bot.x);
        bot.animations.play('walkLeft', 15, true);
    } else if (direction == 'RIGHT') {
        // game.camera.x += dpos;
        // game.camera.x = Math.min(expX, game.camera.x);
        bot.x += dpos;
        bot.x = Math.min(expX, bot.x);
        bot.animations.play('walkRight', 15, true);
    } else if (direction == 'UP') {
        // game.camera.y -= dpos;
        // game.camera.y = Math.max(expY, game.camera.y);
        bot.y -= dpos;
        bot.y = Math.max(expY, bot.y);
        bot.animations.play('walkUp', 15, true);
    } else if (direction == 'DOWN') {
        // game.camera.y += dpos;
        // game.camera.y = Math.min(expY, game.camera.y);
        bot.y += dpos;
        bot.y = Math.min(expY, bot.y);
        bot.animations.play('walkDown', 15, true);
    } else {
        expX = bot.x;
        expY = bot.y;
    }
}
