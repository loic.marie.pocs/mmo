const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)

const exec = require('child_process').execSync;
const CronJob = require('cron').CronJob;
const fs = require('fs');

const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/css', express.static(__dirname + '/css'))
app.use('/js', express.static(__dirname + '/js'))
app.use('/assets', express.static(__dirname + '/assets'))
app.use('/phaser', express.static(__dirname + '/node_modules/phaser-ce/build'))
app.use('/codemirror', express.static(__dirname + '/node_modules/codemirror'))
app.use('/socketio-client', express.static(__dirname + '/node_modules/socket.io-client/dist'))

// var mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost/test');
// var Schema = mongoose.Schema;
//
// // create a schema
// var functionSchema = new Schema({
//   x: String
// });
//
// // the schema is useless so far
// // we need to create a model using it
// var Function = mongoose.model('Function', functionSchema);
//
// // get all the users
// Function.find({}, function(err, functions) {
//   if (err) throw err;
//   // object of all the users
//   console.log(functions[0].x);
// });
var i=0;
io.on('connection', function(client) {
    console.log('Client connected...');
    new CronJob('*/4 * * * * *', function() {
        var out;
        try {
          out = exec('docker exec ' + process.env.CONTAINER_ID + ' python /app/script.py').toString('utf8');
        } catch(e) {
          console.error(e);
          out = 'STOP';
        }
        client.emit('action', out)
        i++;
    }, null, true, 'America/Los_Angeles');
});

app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html');
});

app.get('/function/get', function(req,res) {
    var out = fs.readFileSync('./scripts/script.py').toString('utf8');
    res.json({function: out});
});

app.post('/function/update',function(req,res){
    fs.writeFileSync('./scripts/script.py', req.body.function);
    res.status(200);
});

// app.get('/action',function(req,res){
//     try {
//       var out = exec('docker exec 4f899f381ce1 python /app/script.py');
//       res.json({ action: out });
//     } catch(e) {
//       console.error(e);
//       res.status(300);
//     }
// });

app.listen(3000);
server.listen(4200);
